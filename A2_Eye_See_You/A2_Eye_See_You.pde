/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;
import gab.opencv.*;
// to get Java Rectangle type
import java.awt.*; 
Capture cam;
OpenCV opencv;
// scale factor to downsample frame for processing 
float scale = 0.5;
// image to display
PImage output;
PImage eye;
// array of bounding boxes for face
Rectangle[] faces;
PGraphics circle;
int timer = 0;
float Hx1 = 500;
float Hx2 = 330;
float Hy = 250;
void setup() {
  //size(640, 480);
  fullScreen();
  background(#f4ad42);
  circle = createGraphics(40, 40);
  // want video frame and opencv proccessing to same size
  //cam = new Capture(this, int(640 * scale), int(480 * scale));
  cam = new Capture(this, int(640 * scale), int (480 * scale), "HD Pro Webcam C920"
    , 30);
  opencv = new OpenCV(this, cam.width, cam.height);
  opencv.loadCascade(OpenCV.CASCADE_EYE);  
  cam.start();
  // init to empty image
  output = new PImage(cam.width, cam.height);
  eye = new PImage(0, 0);
}
void draw() {
  strokeWeight(30);
  //fill(40, 255, 40);
  fill(#f4ad42);
  rect(Hx1, Hy, Hx2, Hy);
  if (cam.available() == true) {
    cam.read();
    // load frame into OpenCV 
    opencv.loadImage(cam);
    // it's often useful to mirror image to make interaction easier
    // 1 = mirror image along x
    // 0 = mirror image along y
    // -1 = mirror x and y
    opencv.flip(1);
    faces = opencv.detect();
    // switch to RGB mode before we grab the image to display
    opencv.useColor(RGB);
    output = opencv.getSnapshot();
  }
  // draw the image
  pushMatrix();
  translate(width/3 + 50, height/3);
  scale(1); // inverse of the downsample scale
  image(output, 0, 0 );
  popMatrix();
  // draw face tracking debug
  if (faces != null && faces.length > 0) {
    for (int i = 0; i < 1; i++) {
      // scale the tracked faces to canvas size
      float s = 1 / scale;
      int x = int(faces[i].x * s);
      int y = int(faces[i].y * s);
      int w = int(faces[i].width * s);
      int h = int(faces[i].height * s);
      // draw bounding box and a "face id"
      //stroke(255, 255, 0);
      //noFill();     
      //rect(x, y, w, h);
      //fill(255, 255, 0);
      //text(i, x, y - 20);
    }
  }
  //fill(255, 0, 0);
  // nfc is a function to format numbers, second argument is 
  // number of decimal places to show
  //text(nfc(frameRate, 1), 20, 20);

  if (faces != null && faces.length > 0) {
    for (int i = 0; i < faces.length; i++) {
      Rectangle r = faces[i];

      eye = new PImage(r.width, r.height);
      println(r.x, r.y, r.width, r.height);
      eye.copy(cam, cam.width - r.x - r.width, r.y, r.width, r.height, 0, 0, r.width, r.height);
    }
  }
  float xR = random(width);
  float yR = random(height);
  if (xR != Hx1 && xR != Hx2 && yR != Hy) {
    //if (timer == 5) { 
      pushMatrix();
      //imageMode(CENTER);
      scale(4);
      translate(xR, yR);
      image(eye, 0, 0);
      popMatrix();
      if (timer == 5) {
      for (int i = 0; i < 10; i++) {
        float ly = random(0, height);
        //float lx = random(0, width);
        //stroke(0, 0, 255, 10);
        //line(lx, ly, lx, ly);
        stroke(random(0, 255), random(0, 255), random(0, 255), 20);
        line(0, ly, width, ly);
      }
    }
  }
  if (timer < 10) {
    timer++;
  } else {
    timer = 0;
  }
}

void keyPressed() {
  background(#f4ad42);
}
