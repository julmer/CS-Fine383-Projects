
class Agent {

  // current position
  float x, y;
  float px, py;
  // previous position
  float dx, dy;
  // starting position
  float sx, sy;
  // if a node grows
  boolean grow = false;




  // create agent that picks starting position itself
  Agent(float xx, float yy, float ddx, float ddy) {
    // default is all agent are at centre
    x = xx;
    y = yy;
    dx = ddx;
    dy = ddy;

    sx = x;
    sy = y;

    // pick a new position
    dx = dx + random(-0.1, 0.1);
    dy = dy + random(-0.1, -1);
  }

  float level = 0.03;
  boolean dead = false;


  void node() {
    ellipse(px, py, 30, 30);
  }
  void update() {
    float bx = random(x, px);
    float by = random(y, py);
    if (y < 0) {
      dead = true;
    }
    float stock = 100;

    if (dist(x, y, sx, sy) < stock) {
      // save last position
      px = x;
      py = y;
      x = x + dx;
      y = y + dy;

      if ((random(1) < level) && (dy < y) && (dx < x)) {
        if (dy < sy) {


          Agent a = new Agent(bx, by, random(dy, -dy), random(dx) );
          if ((dy < py)) {
            agents.add(a);


            grow = true;
            if (level > 0.0001) {
              level = -0.00001;
              stock = +10;
            }
          }
        }
      } 
    } else {
      //dead = true;
    }
  }


  void draw() {
    // draw a line between last position
    // and current position
    if (dead == false) {
      strokeWeight(6);
      stroke(random(0, 50), random(50, 200), random(0, 50));
      line(px, py, x, y);
    }
  }
}
