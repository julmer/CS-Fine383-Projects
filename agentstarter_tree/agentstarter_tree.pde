/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

//Gui gui;

// list of agents
ArrayList<Agent> agents;
ArrayList<ArrayList<Agent>> plamts;

int agentsCount;

// add your agent parameters here
float param = 1;
float opacity = 20;
float maxWeight = 1;

void setup() {
  //size(800, 600);
  fullScreen();
  background(0);
  agentsCount = 1;

  // setup the simple Gui
  //gui = new Gui(this);

  //gui.addSlider("agentsCount", 10, height);
  //gui.addSlider("param", 0, 5);
  //gui.addSlider("opacity", 0, 255);
  //gui.addSlider("maxWeight", 1, 100);
  //gui.addSlider("colour", 20, 200);

  plamts = new  ArrayList< ArrayList<Agent>>();

  createAgents();
}

void createAgents() {

  //background(200, 200, 100);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  plamts.add( new ArrayList<Agent>());
  //for (int i = 0; i< plamts.size(); i++) {
    if (random(1) > 0.05) {
      Agent a = new Agent(mouseX, height, 0, -1);

      agents.add(a);
    }
  //}
}

void draw() {

  int d = -1;

  // update all agents
  // draw all the agents
  for (int i = 0; i < agents.size(); i++ ) {
    Agent a = agents.get(i);
    a.update();
    if (a.dead == true) {
      d = i;
    }
  }

  if (d >= 0) {
    println("dead ", d);
    //d.node();
    agents.remove(d);
  }

  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }
  for (int i = 0; i < 5; i++) {
    float ly = random(0, height);
    //float lx = random(0, width);
    //stroke(0, 0, 255, 10);
    //line(lx, ly, lx, ly);
    stroke(0, 5);
    line(0, ly, width, ly);
  }

  // draw Gui last
  //gui.draw(); 

  // interactively adjust agent parameters
  //param = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  //gui.keyPressed(); 

  // space to reset all agents
  if (key == ' ') {
    background(0);
    createAgents();
  }
  //if (key == 'g') {
  //  for (int i = 0; i < agents.size(); i++ ) {
  //    Agent b = agents.get(i);
  //    b.update();
  //  }

  //  // draw all the agents
  //  for (Agent b : agents) {
  //    b.draw();
  //  }
  //}
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n; 
  createAgents();
}

void mousePressed() {
createAgents();
}
