
class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  
  float shade;
  
  float weight;
  
  float colour;


  // create agent that picks starting position itself
  Agent() {
    // default is all agent are at centre
    x = width / 2;
    y = height / 2;
    shade = 255 * int(random(0, 2));
    weight = (random(1, maxWeight));
   
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  void update() {
    // save last position
    px = x;
    py = y;

    // pick a new position
    x = x + random(-param, param);
    y = y + random(-param, param);

  }


  void draw() {

    // draw a line between last position
    // and current position
    strokeWeight(weight);
    stroke(shade, opacity, colour);
    line(px, py, x, y);
  }
}
